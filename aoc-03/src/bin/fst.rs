use std::fs::File;
use std::io::prelude::*;
use std::cmp;

fn main() {
    let filename = "input.txt";
    let mut f = File::open(filename).expect("file not found");
    let mut contents = String::new();
    let mut grid = vec![vec![0; 1000]; 1000];

    f.read_to_string(&mut contents).expect("Something went wrong with opening the file");
    for line in contents.lines() {
        let (x, y, delta_x, delta_y) = parse_params(line);
        for i in x..cmp::min(x + delta_x, 1000) {
            for j in y..cmp::min(y + delta_y, 1000) {
                grid[i][j] += 1;
            }
        }
    }
    let total = count_totals(grid);
    println!("Area totals: {}", total);
}

fn parse_params(line: &str) -> (usize, usize, usize, usize) {
    let parts = line.split_whitespace().collect::<Vec<&str>>();
    let mut offset_str = String::from(parts[2]);
    offset_str.pop();
    let offset = offset_str.split(',').collect::<Vec<&str>>();
    let magnitude = parts[3].split('x').collect::<Vec<&str>>();

    let (x,y) = (offset[0].parse::<usize>().unwrap(), offset[1].parse::<usize>().unwrap());
    let (delta_x, delta_y) = (magnitude[0].parse::<usize>().unwrap(), magnitude[1].parse::<usize>().unwrap());
    (x, y, delta_x, delta_y)
}

fn count_totals(grid: Vec<Vec<usize>>) -> u32 {
    let mut total = 0;
    for row in grid.iter() {
        for elem in row.iter() {
            if *elem > 1 {
                total += 1;
            }
        }
    }
    total
}
