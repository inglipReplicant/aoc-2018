use std::fs::File;
use std::io::prelude::*;
use std::collections::{BinaryHeap, HashMap, HashSet};
use std::cmp::Ordering;
use std::fmt;

struct Node {
    index: String,
    prereq: Vec<String>,
}

impl Ord for Node {
    fn cmp(&self, other: &Node) -> Ordering {
        (other.prereq.len(), &other.index).cmp(&(self.prereq.len(), &self.index))
    }
}

impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Node) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Node {
    fn eq (&self, other: &Node) -> bool {
        self.index == other.index && self.prereq.len() == other.prereq.len()
    }
}

impl Eq for Node { }

impl fmt::Display for Node {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.index, self.prereq.len())
    }
}

fn main() {
    let filename = "test_input.txt";
    let mut f = File::open(filename).expect("file not found");
    let mut contents = String::new();

    f.read_to_string(&mut contents).expect("Something went wrong with opening the file");

    let mut nodes = parse_input(&contents).into_sorted_vec();
    //prvi dio
    {
        let mut order = String::new();
        while !nodes.is_empty() {
            let head = nodes.pop().unwrap();
            order.push_str(&head.index.clone());
            //println!("{}", head);
            for mut node in nodes.iter_mut() {
                let ind = find_index(&node, &head.index);
                let requisites: &mut Vec<String> = &mut node.prereq;
                if ind > requisites.len() {
                    continue;
                }
                requisites.remove(ind);
            }
            nodes.sort_unstable();
            for n in nodes.iter().rev() {
                print!("{}({})", n.index, n.prereq.len());
            }
            println!();
        }
        println!("{}", order);
    }
}

fn parse_input(lines: &String) -> BinaryHeap<Node> {
    let mut prereqs = HashMap::new();
    let mut node_indices = HashSet::new();
    for line in lines.lines() {
        let (from, to) = parse_line(line);
        let index = prereqs.entry(to.clone()).or_insert(Vec::new());
        index.push(from.clone());
        node_indices.insert(from);
        node_indices.insert(to);
    }

    for node_ind in node_indices.iter() {
        let _ = prereqs.entry(node_ind.to_string()).or_insert(Vec::new());
    }

    prereqs.iter()
           .map(|(ind, reqs)| Node {
               index: ind.to_string(),
               prereq: reqs.to_vec(),
           })
           .collect::<BinaryHeap<Node>>()
}

fn parse_line(line: &str) -> (String, String) {
    let parts = line.split_whitespace().collect::<Vec<&str>>();
    (parts[1].to_string(), parts[7].to_string())
}

fn find_index(node: &Node, name: &String) -> usize {
    let prereq = &node.prereq;
    for (i, p) in (0..).zip(prereq.iter()) {
        if p == name {
            return i;
        }
    }
    usize::max_value()
}
