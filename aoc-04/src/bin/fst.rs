use std::fs::File;
use std::io::prelude::*;
use std::collections::HashMap;

fn main() {
    let filename = "input.txt";
    let mut f = File::open(filename).expect("file not found");
    let mut contents = String::new();
    let mut guard_time = HashMap::new();

    f.read_to_string(&mut contents).expect("Something went wrong with opening the file");
    let mut sorted_strings = contents.lines().collect::<Vec<&str>>();
    sorted_strings.sort_unstable();

    let mut string_iter = sorted_strings.into_iter().peekable();
    while string_iter.peek() != None {
        let next = string_iter.next().unwrap();
        let guard: String = extract_name(next);

        while string_iter.peek() != None && !string_iter.peek().unwrap().contains("Guard") {
            let start_minute = extract_minute(string_iter.next().unwrap());
            let end_minute = extract_minute(string_iter.next().unwrap());

            let minutes = guard_time.entry(guard.clone()).or_insert(vec![0; 60]);
            for min in start_minute..end_minute {
                minutes[min as usize] += 1;
            }
        }
    }

    let (max, sleepiest_guard) = guard_time.iter()
                                           .map(|(g, t)| (t.iter().fold(0, |acc, x| acc + x), g.to_string()))
                                           .max()
                                           .unwrap();

    let sleepiest_min = get_minute_index(&mut guard_time, sleepiest_guard.to_string());
    println!("Sleepiest guard is {}, who slept for {} minutes, mostly during minute {}",
                 sleepiest_guard, max, sleepiest_min);
}

fn extract_minute(line: &str) -> u32 {
    let parts = line.split_at(15);
    let mins: String = parts.1.trim().chars().take(2).collect();
    mins.parse::<u32>().unwrap()
}

fn get_minute_index(guard_time: &mut HashMap<String, Vec<u32>>, guard: String) -> u32 {
    let mut most_slept = 0;
    let mut sleepiest_min = 0;
    let mut ind = 0;
    let min_vector = guard_time.entry(guard.to_string()).or_insert(vec![0; 1]);
    for min in min_vector.into_iter() {       
        if *min > most_slept {
            most_slept = *min;
            sleepiest_min = ind;
        }
        ind += 1;
    }
    sleepiest_min
}

fn extract_name(line: &str) -> String {
    let parts = line.split_whitespace().collect::<Vec<&str>>();
    String::from(parts[3])
}
