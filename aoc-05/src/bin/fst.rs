use std::fs::File;
use std::io::prelude::*;

fn main() {
    let filename = "input.txt";
    let mut f = File::open(filename).expect("file not found");
    let mut contents = String::new();

    f.read_to_string(&mut contents).expect("Something went wrong with opening the file");
    let mut contents = contents.trim().chars().collect::<Vec<char>>();
    let mut changed = true;
    let mut indexlen = contents.len() - 1;

    while changed {
        changed = false;
        for i in 0..indexlen {
            if opposite_cases(contents[i], contents[i + 1]) {
                contents.remove(i + 1);
                contents.remove(i);
                changed = true;
                indexlen -= 2;
                break;
            }
        }
    }
    let res: String = contents.into_iter().collect();
    println!("{}:(len {})", res, res.len());
}

fn opposite_cases(fst: char, snd: char) -> bool {
    let fst_upper = fst.to_uppercase().collect::<Vec<_>>();
    let fst_lower = fst.to_lowercase().collect::<Vec<_>>();
    if fst.is_uppercase() && (snd == fst_lower[0]) ||
        fst.is_lowercase() && (snd == fst_upper[0]) {
            return true;
    }
    false
}
