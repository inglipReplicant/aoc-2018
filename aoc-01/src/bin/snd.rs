use std::io;
use std::io::prelude::*;
use std::collections::HashMap;

struct Change {
    op: char,
    val: i32,
}

fn main() {
    let stdin = io::stdin();
    let mut total = 0;
    let mut changes: Vec<Change> = Vec::new();
    let mut twice = false;
    let mut seen: HashMap<i32, i32> = HashMap::new();
    let mut repeating = 0;

    let res = stdin.lock().lines().map(|l| l.unwrap());
    for mut r in res {
       changes.push(Change {
           op: r.remove(0),
           val: r.parse::<i32>().unwrap(),
       });
    }

    while !twice {
        for change in changes.to_vec().into_iter() {
            if change.op == '+' {
                total = total + change.val;
                if seen.contains_key(&total) {
                    repeating = total;
                    twice = true;
                    break;
                } else {
                    seen.insert(total, 1);
                }
            } else {
                total = total - change.val;
                if seen.contains_key(&total) {
                    repeating = total;
                    twice = true;
                    break;
                } else {
                    seen.insert(total, 1);
                }
            }
        }
    }
    println!("Repeating value is {}", repeating);
}

impl Clone for Change {
    fn clone(&self) -> Change {
        Change {
            op: self.op,
            val: self.val,
        }
    }
}
