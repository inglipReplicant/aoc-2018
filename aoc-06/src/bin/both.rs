use std::fs::File;
use std::io::prelude::*;
use std::fmt;
use std::collections::HashMap;

struct Point {
    x: i32,
    y: i32
}

fn main() {
    let filename = "input.txt";
    let mut f = File::open(filename).expect("file not found");
    let mut contents = String::new();
    f.read_to_string(&mut contents).expect("Something went wrong with opening the file");

    let points: Vec<(usize, Point)> = contents.lines()
        .map(|l|{
            let parts = l.split(',').collect::<Vec<&str>>();
            return Point {
                x: parts[0].trim().parse().unwrap(),
                y: parts[1].trim().parse().unwrap()
            };
        })
        .enumerate()
        .collect();

    //prvi dio
    {
        let indexed_pts = handle_pts(&points, manhattan_all);
        let mut indices: Vec<String> = indexed_pts.iter().map(|(ind, _)| ind.to_string()).filter(|c| c != ".").collect();
        indices.sort_unstable();
        let mut map = HashMap::new();
        for ind in indices.iter() {
            let c = map.entry(ind).or_insert(0);
            *c += 1;
        }
        let max_index = map.iter().map(|(_, v)| v).max().unwrap();
        println!("Maximum repetitions of an element: {}", max_index);
    }

    //drugi dio
    {
        let all_totals = handle_pts(&points, manhattan_from_all);
        let under_limit = all_totals.iter()
                                    .map(|(dist_str, _)| dist_str.parse::<u32>().unwrap())
                                    .filter(|x| *x < 10000)
                                    .count();
        println!("Number of points: {}", under_limit);
    }
}

fn handle_pts<F>(points: &Vec<(usize, Point)>, fun: F) -> Vec<(String, Point)> 
    where F: Fn(&Point, &Vec<(usize, Point)>) -> String 
    {
        let (top_left, bot_right) = calc_boundaries(points);
        println!("Top left: {}, bottom right: {}", top_left, bot_right);
        let mut indexed_pts: Vec<(String, Point)> = Vec::new();

        for i in top_left.x..bot_right.x + 1 {
            for j in top_left.y..bot_right.y + 1 {
                let pt = Point {
                    x: i,
                    y: j
                };
                let closest = fun(&pt, points);
                indexed_pts.push((closest, pt));
            }
        }
        return indexed_pts;
}

fn calc_boundaries(points: &Vec<(usize, Point)>) -> (Point, Point) {
    let top_left = Point {
        x: points.iter().min_by_key(|(_, pt)| pt.x).unwrap().1.x,
        y: points.iter().min_by_key(|(_, pt)| pt.y).unwrap().1.y
    };
    let bot_right = Point {
        x: points.iter().max_by_key(|(_, pt)| pt.x).unwrap().1.x,
        y: points.iter().max_by_key(|(_, pt)| pt.y).unwrap().1.y
    };
    (top_left, bot_right)
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

impl Point {
    fn manhattan(&self, pt: &Point) -> i32 {
        (self.x - pt.x).abs() + (self.y - pt.y).abs()
    }
}

fn manhattan_all(pt: &Point, pts: &Vec<(usize, Point)>) -> String {
    let mut closest = String::new();
    let mut min_distance = i32::max_value();

    for (index, other) in pts.iter() {
        let manhattan_distance = pt.manhattan(other);
        //println!("Distance from {} to {} is {}", self, pt, manhattan_distance);
        if manhattan_distance < min_distance {
            min_distance = manhattan_distance;
            closest = index.to_string(); 
        } else if manhattan_distance == min_distance {
            closest = ".".to_string();
        }
    }
    return closest;
}

fn manhattan_from_all(pt: &Point, pts: &Vec<(usize, Point)>) -> String {
    let mut total_distance = 0;
    for (_, other) in pts.iter() {
        total_distance += pt.manhattan(other);
    }
    total_distance.to_string()
}
