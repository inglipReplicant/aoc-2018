use std::fs::File;
use std::io::prelude::*;
use std::collections::HashMap;

fn main() {
    let filename = "input.txt";
    let mut f = File::open(filename).expect("file not found");
    let mut contents = String::new();
    let mut seen: [u32; 2] = [0, 0];

    f.read_to_string(&mut contents).expect("Something went wrong with opening the file");
    for line in contents.trim().split('\n') {
        let mut charcount = count_characters(line); 
        
        let mut vals: Vec<u32> = charcount.values()
                                        .filter(|cnt| **cnt == 2 || **cnt == 3)
                                        .map(|x| return *x)
                                        .collect();
        if vals.contains(&2) {
            seen[0] += 1;
        }
        if vals.contains(&3) {
            seen[1] += 1;
        }
    }
    let checksum = seen[0] * seen[1];
    println!("Total checksum is {}", checksum);
}

fn count_characters(line: &str) -> HashMap<char, u32> {
    let mut chars = HashMap::new();
    for ch in line.chars() {
        let ctr = chars.entry(ch).or_insert(0);
        *ctr += 1;
    }
    chars
}
