use std::fs::File;
use std::io::prelude::*;

fn main() {
    let filename = "input.txt";
    let mut f = File::open(filename).expect("File not found");
    let mut contents = String::new();

    f.read_to_string(&mut contents).expect("Something went wrong with opening the file");
    for line in contents.trim().split('\n') {
        for other in contents.trim().split('\n') {
            let mut diff = 0;
            let mut same_str = String::new();
            for (mine, theirs) in line.chars().zip(other.chars()) {
                if mine == theirs {
                    same_str.push(mine);
                } else {
                    diff += 1;
                }
                if diff > 1 {
                    continue;
                }
            }
            if diff == 1 {
                println!("ID: {}", same_str);
            }
        }
    }
}
